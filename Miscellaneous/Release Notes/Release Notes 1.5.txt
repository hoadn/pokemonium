[FIX] Gen 4 pokemon not being registrated correctly.
[FIX] Slow walking when pokedex is open.
[FIX] Scrolling the pokedex is slow/laggy.
[FIX] Unable to close the pokedex with the '2' hotkey.
[FIX] Trading now works properly.
[FIX] connection between cinnebar island and fushia city
[FIX] invissible (shop)items
[FIX] unsteppable areas
[FIX] invisible NPCs
[FIX] wild exp gain is now based on the participating pokemon levls
[FIX] 4th gen pokemon sprites
[FIX] game crash on ditto, smeargle and some legendaries
[FIX] fishing bug
[FIX] faster server boot
[FIX] shed skin
[FIX] status ailments display in battle
[FIX] stat lowering moves now show they do
[FIX] several abilities now show what they do
[FIX] recalibrate player position on end of battle
[FIX] updated pokemon moves
[FIX] no outer collision in 8th gym of kanto
[FIX] bigger bag

Remade the entire client/server communication infrastructure.

[ADD] Hoenn and Sinnoh (includes caves, gyms, pokemon centers, pokemarkts)
[ADD] Kanto cycle route
[ADD] train and boat transportation
[reADD] Kanto safari zone
[ADD] entry control on Mt.Silver and safari zone
[ADD] some of slams sprites are now buyable ingame
[ADD] Kanto & Johto graphic update
[ADD] new sprites
[ADD] Italian language (if you want your own language to be added provide us with the translated files)
[ADD] new world map
[ADD] kanto elite 4
[ADD] new music

([ADD] Sevii islands, iron island, battlefrontier - staff area)